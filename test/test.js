const { assert } = require('chai');
const { newUser } = require('../index.js')

// describe() gives structure to your test suite
describe('Test newUser object', () => {
    it('Assert newUser type is object', () => {
        assert.equal(typeof(newUser), 'object')
    });
    
    it('Assert newUser.email is type string', () => {
        assert.equal(typeof(newUser.email), 'string')
    });

    it('Assert newUser.email is not undefined', () => {
        assert.notEqual(typeof(newUser.email), undefined)
    });

    it('Assert newUser.password is type string', () => {
        assert.equal(typeof(newUser.password), 'string')
    });

    it('Assert newUser.password.length is greater than or equal to 16', () => {
        assert.isAtLeast(newUser.password.length, 16)
    });

    //Activity Solutions
    //1
    it('Assert newUser.firstName is type string', () => {
        assert.equal(typeof(newUser.firstName), 'string')
    });
    //2
    it('Assert newUser.lastName is type string', () => {
        assert.equal(typeof(newUser.lastName), 'string')
    });
    //3
    it('Assert newUser.firstName is not undefined', () => {
        assert.notEqual(typeof(newUser.firstName), undefined)
    });
    //4
    it('Assert newUser.lastName is not undefined', () => {
        assert.notEqual(typeof(newUser.lastName), undefined)
    });
    //5
    it('Assert newUser.age is at least 18', () => {
        assert.isAtLeast(newUser.age, 18)
    });
    //6
    it('Assert newUser.age is type number', () => {
        assert.equal(typeof(newUser.age), 'number')
    });
    //7
    it('Assert newUser.contactNo is type string', () => {
        assert.equal(typeof(newUser.contactNo), 'string')
    });
    //8
    it('Assert newUser.batchNo is type number', () => {
        assert.equal(typeof(newUser.batchNo), 'number')
    });
    //9
    it('Assert newUser.batchNo is not undefined', () => {
        assert.notEqual(typeof(newUser.batchNo), undefined)
    });
});