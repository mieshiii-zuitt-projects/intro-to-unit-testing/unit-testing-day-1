const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

const newUser = {
    firstName: 'Juan',
    lastName: 'Dela Cruz',
    age: 45,
    contactNo: '09123456789',
    batchNo: 151,
    email: 'juan.delacruz@gmail.com',
    password: 'iamtheJuanDelaCruz'
};

module.exports = { 
    newUser: newUser 
};

app.listen(port, () => console.log(`Server running at port ${port}`));
